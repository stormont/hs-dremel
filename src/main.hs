


data Range = Required
           | Optional
           | Repeated
           deriving (Show,Read,Eq,Ord)


getDefinitionLevel :: [Maybe Range] -> Int
getDefinitionLevel xs = f 0 xs
   where f c []                   = c
         f c (Nothing : _)        = c
         f c (Just Required : xs) = f c xs
         f c (_ : xs)             = f (c + 1) xs


type Key   = String
type Value = String


data Field = AtomicField { atomicFieldKey     :: Key
                         , atomicFieldRange   :: Range
                         , atomicFieldValue   :: Value
                         }
           | GroupField  { groupFieldKey      :: Key
                         , groupFieldRange    :: Range
                         , groupFieldChildren :: [Field]
                         }
           deriving (Show,Read,Eq)


getFieldKey (AtomicField f _ _) = f
getFieldKey (GroupField  f _ _) = f


getFieldRange (AtomicField _ r _) = r
getFieldRange (GroupField  _ r _) = r


isAtomicField (AtomicField _ _ _) = True
isAtomicField _                   = False


getRepetitionLevel :: [Field] -> [Field] -> Int
getRepetitionLevel prevField curField = f 0 prevField curField
   where f c []     _                            = c
         f c _      []                           = c
         f c (x:xs) (y:ys)
            | (getFieldKey x) /= (getFieldKey y) = c
            | (getFieldRange y) /= Repeated      = f c       xs ys
            | otherwise                          = f (c + 1) xs ys


-------------------------------------
-- The below is just a stub for now
-------------------------------------


getFieldChild x field = undefined
getFieldID field = undefined
getFieldDepth field = undefined
isFieldAtomic field = undefined
updateFieldLevels repLevel field = undefined
getNestedDecoder decoder = undefined


dissect field _   _        _         []     = return ()
dissect field ids repLevel writeFunc (x:xs) = do
   let chField = getFieldChild x field
       chFieldID = getFieldID chField
       (chRepLevel,ids') = if elem chFieldID ids
                             then (getFieldDepth chField,ids)
                             else (repLevel,chFieldID : ids)
   if isFieldAtomic chField
     then do
      writeFunc x repLevel chField
      dissect field ids repLevel writeFunc xs
     else dissectRecord chField chRepLevel writeFunc $ getNestedDecoder x


dissectRecord field repLevel writeFunc decoder = do
   let field' = updateFieldLevels repLevel field
   dissect field' [] repLevel writeFunc decoder
